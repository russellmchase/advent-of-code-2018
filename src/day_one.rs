use std::fs;
use std::collections::HashSet;

pub fn exec_part_one()
{
    let content = fs::read_to_string("day_one.txt").expect("Something went wrong reading the file");

    let split = content.split_whitespace();

    let mut result = 0;

    for lexicon in split
    {
        let operation = lexicon.chars().next().unwrap();
        let operand : String = lexicon.chars().skip(1).take(lexicon.len()).collect();
        let operand : i32 = operand.parse().unwrap();

        if operation == '+' { result = result + operand}
        else { result = result - operand}
    }
    println!("{}", result);
}

pub fn exec_part_two()
{
    let mut frequencies = HashSet::new();
    let mut result = 0;
    let mut found = false;
    frequencies.insert(result);

    while !found {
        let content = fs::read_to_string("day_one.txt").expect("Something went wrong reading the file");
        let lexicons: Vec<&str> = content.split_whitespace().collect();

        for lexicon in lexicons
        {
            let operation = lexicon.chars().next().unwrap();
            let operand: String = lexicon.chars().skip(1).take(lexicon.len()).collect();
            let operand: i32 = operand.parse().unwrap();

            if operation == '+' { result = result + operand } else { result = result - operand }

            if frequencies.contains(&result)
                {
                    println!("{}", result);
                    found = true;
                    break;
                }
            frequencies.insert(result);
        }
    }
}
