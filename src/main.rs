mod day_one;
mod day_two;

fn main() {
    day_one::exec_part_one();
    day_one::exec_part_two();
    day_two::exec_part_one();
}
