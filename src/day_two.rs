use std::fs;
use std::collections::HashMap;
use std::collections::HashSet;
use std::ops::AddAssign;

pub fn exec_part_one()
{
    let content = fs::read_to_string("day_two.txt").expect("Something went wrong reading the file");
    let split = content.split_whitespace();

    let mut two_count = 0;
    let mut three_count = 0;

    for id in split {

        let mut letters : HashMap<char, i32> = HashMap::new();
        let mut letter_counts = HashSet::new();

        for c in id.chars() {
            if letters.contains_key(&c) {
                letters.get_mut(&c).unwrap().add_assign(1);
            } else {
                letters.insert(c, 1);
            }
        }
        for val in letters.values() {
            letter_counts.insert(val);
        }
        if letter_counts.contains(&2) {
            two_count += 1;
        }
        if letter_counts.contains(&3) {
            three_count += 1;
        }
    }
    println!("{}", two_count * three_count);
}

pub fn exec_part_two()
{
    let mut frequencies = HashSet::new();
    let mut result = 0;
    let mut found = false;
    frequencies.insert(result);

    while !found {
        let content = fs::read_to_string("day_one.txt").expect("Something went wrong reading the file");
        let lexicons: Vec<&str> = content.split_whitespace().collect();

        for lexicon in lexicons {
            let operation = lexicon.chars().next().unwrap();
            let operand: String = lexicon.chars().skip(1).take(lexicon.len()).collect();
            let operand: i32 = operand.parse().unwrap();

            if operation == '+' { result = result + operand } else { result = result - operand }

            if frequencies.contains(&result) {
                println!("{}", result);
                found = true;
                break;
            }
            frequencies.insert(result);
        }
    }
}
